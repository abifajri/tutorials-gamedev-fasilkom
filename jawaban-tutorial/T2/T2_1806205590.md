## **Tutorial 2**

**Nama: Abi Fajri Abdillah  
NPM: 1806205590**

 1. Scene BlueShip dan StonePlatform sama-sama memiliki sebuah child node bertipe Sprite. Apa fungsi dari node bertipe Sprite?  
**Jawab:** Node bertipe sprite berfungsi untuk menampilkan 2D texture. Atau dengan kata lain, berfungsi sebagai node yang akan menampilkan sprite (aset gambar) dari objek tersebut.

 2. Root node dari scene BlueShip dan StonePlatform menggunakan tipe yang berbeda. BlueShip menggunakan tipe RigidBody2D, sedangkan StonePlatform menggunakan tipe StaticBody2D. Apa perbedaan dari masing-masing tipe node?  
**Jawab:** RigidBody2D adalah turunan dari PhysicsBody2D yang diperuntukkan untuk objek-objek yang dapat bergerak dengan mengaplikasikan gaya terhadap objek tersebut, sedangkan StaticBody2D adalah turunan dari PhysicsBody2D yang diperuntukkan untuk objek-objek tidak bergerak (statis).

  3. Ubah nilai atribut Mass dan Weight pada tipe RigidBody2D secara bebas di scene BlueShip, lalu coba jalankan scene Main. Apa yang terjadi?  
**Jawab:** Yang terjadi adalah objek sprite dari BlueShip turun ke bawah sampai menyentuh StonePlatform. Hal ini karena adanya gaya gravitasi yang diterapkan oleh RigidBody2D.

 4. Ubah nilai atribut Disabled pada tipe CollisionShape2D di scene StonePlatform, lalu coba jalankan scene Main. Apa yang terjadi?  
**Jawab:** BlueShip yang jatuh ke bawah tidak collide dengan StonePlatform karena dengan menonaktifkan CollisionShape2D di objek StonePlatform, objek StonePlatform jadi tidak bisa collide dengan objek lain.

 5. Pada scene Main, coba manipulasi atribut Position, Rotation, dan Scale milik node BlueShip secara bebas. Apa yang terjadi pada visualisasi BlueShip di Viewport?  
**Jawab:** Visualisasi BlueShip di Viewport berubah sesuai apa yang diubah. Misal yang diubah atribut Position, maka visualisasi yang berubah pada Viewport adalah posisi dari BlueShip. Hal iini berlaku juga untuk Rotation dan Scale.

 6. Pada scene Main, perhatikan nilai atribut Position node PlatformBlue, StonePlatform, dan StonePlatform2. Mengapa nilai Position node StonePlatform dan StonePlatform2 tidak sesuai dengan posisinya di dalam scene (menurut Inspector) namun visualisasinya berada di posisi yang tepat?  
**Jawab:** Karena node StonePlatform dan StonePlatform2 adalah child dari PlatformBlue, maka posisi dari kedua node tersebut adalah posisi relatif dari PlatformBlue.
